<img width="300px" src="https://github.com/rafinskipg/git-changelog/raw/master/images/git-changelog-logo.png" />

# Git changelog

_Git changelog is a utility tool for generating changelogs. It is free and opensource. :)_



## Bug Fixes
  - Incorrect settings on grunt:git_changelog:multitask_commits task.
  ([86cd985b](https://TheNonman@bitbucket.org/TheNonman/git-changelog_pf/commits/86cd985b07919bb9c57d1a5fc35c06e6d06909e5))




## Features
  - MultiTask commit and Untyped commit support both made options (added to options data with default FALSE). Handled through all flows (grunt tasks, .changelogrc, and CLI).
  ([216a35bf](https://TheNonman@bitbucket.org/TheNonman/git-changelog_pf/commits/216a35bf3327b291a4572d875ba487cb8c46e823))
  - Support for multiple change tags in a single commit.
  ([9b7db98c](https://TheNonman@bitbucket.org/TheNonman/git-changelog_pf/commits/9b7db98cfe997ee615791dba83a4f739fe08c42b))
  - Support inclusion of untagged commits.
  ([5b89f424](https://TheNonman@bitbucket.org/TheNonman/git-changelog_pf/commits/5b89f4242ca606d3403db8ef27fd7676fef5e54e))




## Chore
  - New version
  ([567232d0](https://TheNonman@bitbucket.org/TheNonman/git-changelog_pf/commits/567232d0ee3db9c598c89a2154701dd89f0244e0))

  - **tasks**
    - new Grunt task for branch.
  ([2ffcb79b](https://TheNonman@bitbucket.org/TheNonman/git-changelog_pf/commits/2ffcb79b1739013e1f2cf72e86ed0d5cdfe5a45e))





---
<sub><sup>*Generated with [git-changelog](https://github.com/rafinskipg/git-changelog). If you have any problems or suggestions, create an issue.* :) **Thanks** </sub></sup>
