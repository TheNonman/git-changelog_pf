#!/usr/bin/env bash

# Reference Pages
# https://git-scm.com/docs/git-log/							 <- "Official Documentation"
# https://www.atlassian.com/git/tutorials/git-log			 <- "Advanced Git Log Article"
# https://devhints.io/git-log								 <- "git log Cheatsheet"

# One Line Summary per Commit w/ Graph Streamed to the File, "log.log"
git log --pretty=format:'%h : %s' --graph > log.log

# One Line Summary per Commit w/o Graph Streamed to the File, "log_nograph.log"
git log --pretty=format:'%h was %an, %ar, message: %s' > log_nograph.log

# Full Raw Logs Streamed to the File, "rawlog.log"
git log > rawlog.log

# Full Logs (trimmed to the firstline of the commit message) Streamed to the File, "raw_onelinelog.log"
git log --oneline > raw_onelinelog.log

# Full Logs (trimmed to the firstline of the commit message) Streamed to the File, "raw_onelinelog.log"
git log --oneline > raw_onelinelog.log