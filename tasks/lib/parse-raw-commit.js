'use strict';

var debug = require('debug')('changelog:parseRawCommit');

function parseLine(msg, line) {
  var match = line.match(/(?:Closes|Fixes)\s#(\d+)/);
  if (match) {
    msg.closes.push(parseInt(match[1], 10));
  }
}

function parseLineExtras(msg, line) {
  var handledMatch = line.match(/(?:Closes|Fixes)\s#(\d+)/);
  if (handledMatch) {
    return;
  }

  var additionalMsg = {};
  additionalMsg.closes = [];
  additionalMsg.breaks = [];

  var matchWithComponent = line.match(/^(.*)\((.*)\)\:\s(.*)$/);
  if (!matchWithComponent) {
    var matchNoComponent = line.match(/^(.*)\:\s(.*)$/);
    if (matchNoComponent) {
      additionalMsg.hash = msg.hash;
      additionalMsg.type = matchNoComponent ? matchNoComponent[1] : null;
      additionalMsg.subject = matchNoComponent ? matchNoComponent[2] : msg.subject;

      debug('Additional Tag message [hash]: %o', additionalMsg.hash);
      debug('Additional Tag message [type]: %o', additionalMsg.type);
      debug('Additional Tag message [subject]: %o', additionalMsg.subject);
    }
  }
  else {
    additionalMsg.hash = msg.hash;
    additionalMsg.type = matchWithComponent[1];
    additionalMsg.component = matchWithComponent[2];
    additionalMsg.subject = matchWithComponent[3];

    debug('Additional Tag message [hash]: %o', additionalMsg.hash);
    debug('Additional Tag message [type]: %o', additionalMsg.type);
    debug('Additional Tag message [component]: %o', additionalMsg.component);
    debug('Additional Tag message [subject]: %o', additionalMsg.subject);
  }

  if (additionalMsg.type) {
    debug('Additional Tag message pushed.');
    msg.additions.push(additionalMsg);
  }
}

function parseRawCommit(raw) {
  debug('parsing raw commit');
  const { options } = this || {};

  if (!raw) {
    return null;
  }

  var lines = raw.split('\n');
  var msg = {}, match;

  msg.closes = [];
  msg.breaks = [];

  lines.forEach(parseLine.bind(null, msg));

  msg.hash = lines.shift();
  msg.subject = lines.length > 0 ? lines.shift() : '';

  if (options.multitask_commits) {
    msg.additions = [];
    lines.forEach(parseLineExtras.bind(null, msg));
  }

  match = raw.match(/BREAKING CHANGE:([\s\S]*)/);

  if (match) {
    msg.breaking = match[1];
  }

  msg.body = lines.join('\n');
  match = msg.subject.match(/^(.*)\((.*)\)\:\s(.*)$/);
  //@TODO: match merges and pull request messages
  if (!match) {
    match = msg.subject && msg.subject.match(/^(.*)\:\s(.*)$/);

    if (!match) {
      //console.log(msg.subject, '------------');

      if (options.untyped_commits) {
        msg.type = 'UNTYPED';
        msg.subject = match ? match[2] : msg.subject;

        this.log('warn', 'Incorrect message [hash]:', msg.hash);
        this.log('warn', 'Incorrect message [type]:', msg.type);
        this.log('warn', 'Incorrect message [subject]:', msg.subject);

        return msg;
      }
    }

    msg.type = match ? match[1] : null;
    msg.subject = match ? match[2] : msg.subject;

    //this.log('info', 'Correct message [hash]:', msg.hash);
    //this.log('info', 'Correct message [type]:', msg.type);
    //this.log('info', 'Correct message [subject]:', msg.subject);

    return msg;
  }

  msg.type = match[1];
  msg.component = match[2];
  msg.subject = match[3];

  return msg;
}

module.exports = parseRawCommit;
